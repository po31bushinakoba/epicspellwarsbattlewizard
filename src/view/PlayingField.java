package view;

import model.cards.artifact.AbstractArtifact;
import model.cards.cast.Cast;
import model.players.Player;

import java.util.List;
import java.util.Map;

/**
 * Интерфейс игрового поля
 */
public interface PlayingField {

    /**
     * С этого метода все начинается. Отрисовывается поле и хитпоинты обоих игроков
     */
    void showHitpoint(List<Player> players);

    /**
     * Отрисовка карт у игроков на руках (нужны только карты живого игрока, карты бота не надо). Тут еще нужно убрать все
     * лишнии карты со стола (те что были разыграны в прошлом ходе)
     * @param players игроки
     * @param isTurn если true, значит игрок сейчас может ходить
     */
    void showHand(List<Player> players, boolean isTurn);

    /**
     * Отобразить победителя
     * @param player победитель
     */
    void showWinner(Player player);

    /**
     * Отобразить бросание костей
     * @param numerics лист со значением на каждой из кости
     * @param player кто бросил
     */
    void showRollDices(List<Integer> numerics, Player player);

    /**
     * Отобразить разыгрывание заклинания. Нужно выложить карты из залинание перед разыгравшим его игроком.
     * Внутри класса cast есть все карты, а также игрок которые его разыграл. (было бы не плохо завести окошко с логом и
     * внтури него еще отображать чей ход и все card.getDecsription() его карт.
     * @param cast заклинание
     */
    void showCast(Cast cast);

    /**
     * Вызывает после разыгрывание заклинания, по обновляет хитпоинты игроков
     */
    void showChanges(List<Player> players);

    /**
     * Игра ждет прерывание паузы извне
     */
    void pause();

    /**
     * Выделяется карт n-ая карты в заклинании
     *
     * @param n номер карты начиная с 0
     */
    void selectCardInCast(int n);

    /**
     * Снимаются все выделения с карты в заклинании
     */
    void deselectCardsInCast();

    /**
     * Отобразить какой-то текст
     * @param text
     */
    void showText(String text);

    /**
     * Отобразить артефакт
     * @param artifactsByPlayer мапа игрок - его артефакты
     */
    void showArtifacts(Map<Player, List<AbstractArtifact>> artifactsByPlayer);
}
