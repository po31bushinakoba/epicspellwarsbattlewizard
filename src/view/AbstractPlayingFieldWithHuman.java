package view;

import controller.Game;
import model.cards.cast.CastCard;
import model.players.Human;

import javax.swing.*;

/**
 * Абстрактный класс игрового поля на котором играет один живой человек
 */
abstract public class AbstractPlayingFieldWithHuman extends JFrame implements PlayingField {

    /**
     * метод возвращает живого игрока на поле
     * @return
     */
    abstract protected Human getHumanPlayer();

    /**
     * Метод показывает что живой игрок составил заклинание, вызывается по нажатию кнопки
     */
    protected void humanPlayerFinish(){
        getHumanPlayer().finish();
    }

    /**
     * Метод предоставляет игроку выбор (просто отобразить цифрами несколько вариантов)
     * @param text текст описывающий выбор
     * @param count количество вариантов для выбора
     * @return выбранный вариант (давай делать отсчет вариантов с 0)
     */
    abstract public int showHumanChoice(String text, int count);


    /**
     * Добавляет карту к заклинаню живого игрока. Можно сделать так: все карты, которые на руке игрока хранишь в List.
     * Создаешь 8 кнопок. По нажатию, каждая тянет свою карту из листа и передаёт в этот метод.
     * @param card
     * @return возвращает прошло ли все удачно. если да, то карту можно выделить цветом как-то типо она в заклинании
     */
    protected boolean addCardToHumanPlayerHand(CastCard card){
        return getHumanPlayer().addCard(card);
    }

    /**
     * Удаляет карту из заклинания игрока. Реализовать можно аналогично методу выше
     * @param card
     * @return возвращает прошло ли все удачно. если да, то с карты нужно снять выделение
     */
    protected void removeCardToHumanPlayerHand(CastCard card){
        getHumanPlayer().removeCard(card);
    }

    /**
     * Удаляет карту из заклинания игрока. Реализовать можно аналогично методу выше
     * @param card
     * @return возвращает прошло ли все удачно. если да, то с карты нужно снять выделение
     */
    protected void next(){
        Game.pauseOff();
    }
}
