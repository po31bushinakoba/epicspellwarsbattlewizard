package view;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactUtil;
import model.players.Player;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Created by Evgeniy on 26.03.2017.
 */
public class EnemiesArtifacts extends JFrame {

    JPanel mainPanel = new JPanel();

    public EnemiesArtifacts() {
        ToolTipManager.sharedInstance().setInitialDelay(0);

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        getContentPane().add(new JScrollPane(mainPanel));
        setResizable(false);
        setBounds(330, 40, 0, 0);
        showArtifacts(ArtifactUtil.getAllArtifactsByPlayer());
    }

    public void showArtifacts(Map<Player, List<AbstractArtifact>> artifactsByPlayer) {
        mainPanel.removeAll();
        for (Map.Entry<Player, List<AbstractArtifact>> entry : artifactsByPlayer.entrySet()) {
            JPanel playerPanel = new JPanel();
            playerPanel.setLayout(new BoxLayout(playerPanel, BoxLayout.LINE_AXIS));
            playerPanel.add(new JLabel(entry.getKey().getName() + ":"));
            playerPanel.add(Box.createRigidArea(new Dimension(5, 0)));
            for (AbstractArtifact artifact : entry.getValue()) {
                JLabel jLabel = new JLabel();
                jLabel.setToolTipText(PlayingFieldWithHuman2Player.getArtifactDescription(artifact));
                jLabel.setIcon(PlayingFieldWithHuman2Player.getScaledImageIconForArtifact(artifact.getIcon()));
                playerPanel.add(jLabel);
                playerPanel.add(Box.createRigidArea(new Dimension(5, 0)));
            }
            mainPanel.add(playerPanel);
        }
        pack();
    }
}
