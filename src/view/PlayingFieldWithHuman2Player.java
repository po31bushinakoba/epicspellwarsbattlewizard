package view;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;
import model.cards.artifact.active.ActiveArtifact;
import model.cards.cast.Cast;
import model.players.Human;
import model.players.Player;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Классс игрового поля
 */
public class PlayingFieldWithHuman2Player extends AbstractPlayingFieldWithHuman {

    List<Player> players;
    StringBuilder logText = new StringBuilder();

    private Image noCard;

    Border castBorder = BorderFactory.createLineBorder(Color.RED, 5);

    private JLabel myHitpoint = new JLabel();
    private JLabel botHitpoint = new JLabel();

    List<JToggleButton> cards = Arrays.asList(new JToggleButton(), new JToggleButton(), new JToggleButton(), new JToggleButton(),
            new JToggleButton(), new JToggleButton(), new JToggleButton(), new JToggleButton());

    private JLabel playerRollsDices = new JLabel();
    List<JLabel> labelsCubic = Arrays.asList(new JLabel(), new JLabel(), new JLabel(), new JLabel());

    private JLabel castPlayer = new JLabel();
    List<JLabel> castCards = Arrays.asList(new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()); //TODO в будующем расширить или вообще сделать динамическим
                                                                                                    //Так в процессе заклинания, в него могу добавитсья карты

    private JTextPane log = new JTextPane();
    JScrollPane scrollPane = new JScrollPane(log);

    private volatile JButton nextButton = new JButton("Далее");
    private JButton finishButton = new JButton("В атаку!");

    private JPanel activeArtifactPanel = new JPanel();
    private JPanel passiveArtifactPanel = new JPanel();

    private EnemiesArtifacts enemiesArtifactsFrame = new EnemiesArtifacts();

    public PlayingFieldWithHuman2Player() {
        ToolTipManager.sharedInstance().setInitialDelay(0);

        log.setEditable(false);
        log.setContentType("text/html");

        UIManager.put("ToggleButton.select", Color.BLACK);
        for(JToggleButton jToggleButton : cards)
            SwingUtilities.updateComponentTreeUI(jToggleButton);

        for (int i = 0; i < cards.size(); i++) {
            final int finalI = i;
            cards.get(i).addActionListener(a -> {
                 if(getHumanPlayer().getCardsOnHand().size() <= finalI)
                    cards.get(finalI).setSelected(false);

                if (cards.get(finalI).isSelected()) {
                    if(addCardToHumanPlayerHand(getHumanPlayer().getCardsOnHand().get(finalI))) {
                        logText.append("<font face=\"Arial\" size=\"3\"><b>Выбрана карта:</b> " + getHumanPlayer().getCardsOnHand().get(finalI).getName() + "<br>"
                                + "<b>Описание:</b> "/* + "<br>"*/ + getHumanPlayer().getCardsOnHand().get(finalI).getDescription() + "<br></font>");
                        log.setText(logText.toString());
                    } else {
                        cards.get(finalI).setSelected(false);
                        logText.append("<font face=\"Arial\" size=\"3\"><b>Карта:</b> " + getHumanPlayer().getCardsOnHand().get(finalI).getName()
                                + " не может быть добавлена в каст<br></font>");
                        log.setText(logText.toString());
                    }
                } else {
                    logText.append("<font face=\"Arial\" size=\"3\"><b>Снято выделение с:</b> " + getHumanPlayer().getCardsOnHand().get(finalI).getName() +"<br><font>");
                    log.setText(logText.toString());
                    removeCardToHumanPlayerHand(getHumanPlayer().getCardsOnHand().get(finalI));
                }
            });
        }

        finishButton.addActionListener(e -> {
            int countNotSelectedCards = 0;
            for (JToggleButton card : cards) {
                if (!card.isSelected())
                    countNotSelectedCards++;
            }
            if (countNotSelectedCards == cards.size()) {
                JOptionPane.showMessageDialog(this, "В каст не добавлено ни одной карты!", "Эй! Так нельзя!", 0);
                return;
            }
            else {
                humanPlayerFinish();
                for (JToggleButton card : cards)
                    card.setSelected(false);
            }
            finishButton.setEnabled(false);
        });

        nextButton.setEnabled(false);
        nextButton.addActionListener(e -> {
            next();
            nextButton.setEnabled(false);
        });

        try {
            noCard = ImageIO.read(new File("images/nocard.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(null);

        JLabel activeArtifactLabel = new JLabel("Активные артефакты:");
        activeArtifactLabel.setBounds(330, 10, 130, 15);
        JLabel passiveArtifactLabel = new JLabel("Пассивные артефакты:");
        passiveArtifactLabel.setBounds(330, 105, 1340, 15);

        activeArtifactPanel.setLayout(new BoxLayout(activeArtifactPanel, BoxLayout.LINE_AXIS));
        passiveArtifactPanel.setLayout(new BoxLayout(passiveArtifactPanel, BoxLayout.LINE_AXIS));
        activeArtifactPanel.setBounds(330, 30, 370, 70);
        passiveArtifactPanel.setBounds(330, 125, 465, 70);

        JButton showAllArtifacts = new JButton("<html>Показать<br/>артефакты<br/>всех<br/>игроков<html>");
        showAllArtifacts.setBounds(700, 10, 85, 65);
        showAllArtifacts.addActionListener(e -> enemiesArtifactsFrame.setVisible(true));

        myHitpoint.setBounds(70, 10, 90, 25);
        botHitpoint.setBounds(70, 30, 90, 25);
        playerRollsDices.setBounds(70, 50, 140, 25);
        labelsCubic.get(0).setBounds(70, 80, 50, 50);
        labelsCubic.get(1).setBounds(135, 80, 50, 50);
        labelsCubic.get(2).setBounds(200, 80, 50, 50);
        labelsCubic.get(3).setBounds(265, 80, 50, 50);
        finishButton.setBounds(70, 150, 80, 25);
        nextButton.setBounds(155, 150, 80, 25);

        castPlayer.setBounds(1000, 10, 200, 25);
        castCards.get(0).setBounds(800, 50, 175, 235);
        castCards.get(1).setBounds(975, 50, 175, 235);
        castCards.get(2).setBounds(1150, 50, 175, 235);
        castCards.get(3).setBounds(800, 285, 175, 235);
        castCards.get(4).setBounds(975, 285, 175, 235);

        cards.get(0).setBounds(30, 200, 190, 250);
        cards.get(1).setBounds(220, 200, 190, 250);
        cards.get(2).setBounds(410, 200, 190, 250);
        cards.get(3).setBounds(600, 200, 190, 250);
        cards.get(4).setBounds(30, 450, 190, 250);
        cards.get(5).setBounds(220, 450, 190, 250);
        cards.get(6).setBounds(410, 450, 190, 250);
        cards.get(7).setBounds(600, 450, 190, 250);

        scrollPane.setBounds(800, 520, 550, 180);

        mainPanel.add(activeArtifactLabel);
        mainPanel.add(passiveArtifactLabel);
        mainPanel.add(activeArtifactPanel);
        mainPanel.add(passiveArtifactPanel);
        mainPanel.add(showAllArtifacts);

        mainPanel.add(myHitpoint);
        mainPanel.add(botHitpoint);
        mainPanel.add(playerRollsDices);
        mainPanel.add(labelsCubic.get(0));
        mainPanel.add(labelsCubic.get(1));
        mainPanel.add(labelsCubic.get(2));
        mainPanel.add(labelsCubic.get(3));
        mainPanel.add(finishButton);
        mainPanel.add(nextButton);

        mainPanel.add(castPlayer);
        setNullIconForDice();
        mainPanel.add(castCards.get(0));
        mainPanel.add(castCards.get(1));
        mainPanel.add(castCards.get(2));
        mainPanel.add(castCards.get(3));
        mainPanel.add(castCards.get(4));

        mainPanel.add(cards.get(0));
        mainPanel.add(cards.get(1));
        mainPanel.add(cards.get(2));
        mainPanel.add(cards.get(3));
        mainPanel.add(cards.get(4));
        mainPanel.add(cards.get(5));
        mainPanel.add(cards.get(6));
        mainPanel.add(cards.get(7));

        mainPanel.add(scrollPane);

        getContentPane().add(new JScrollPane(mainPanel));
        setSize(Toolkit.getDefaultToolkit().getScreenSize());
        setExtendedState(MAXIMIZED_BOTH);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    protected Human getHumanPlayer() {
        for (Player player : players) {
            if (player instanceof Human) return (Human) player;
        }
        return null;
    }

    @Override
    public int showHumanChoice(String text, int count) {
        return new JDialog(this, true) {
            private volatile int result = -1;

            public int choice() {
                this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                JPanel panel = new JPanel();
                JPanel panelForText = new JPanel();
                JPanel panelForButton = new JPanel();
                JLabel label = new JLabel(text);
                panelForText.add(label);

                for (int i = 0; i < count; i++) {
                    JButton jButton = new JButton();
                    final int finalI = i;
                    jButton.setText("" + i);
                    jButton.addActionListener(e -> {
                        result = finalI;
                        setVisible(false);
                        dispose();
                    });
                    panelForButton.add(jButton);

                    panelForButton.setLayout(new BoxLayout(panelForButton, BoxLayout.X_AXIS));

                    panel.add(panelForText);
                    panel.add(panelForButton);
                    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
                    getContentPane().add(panel);
                }
                setBounds(350, 40, 420, 170);
                pack();
                setVisible(true);

                while (result == -1) ;
                return result;
            }
        }.choice();
    }

    @Override
    public void showHitpoint(List<Player> players) {
        SwingUtilities.invokeLater(() -> {
            this.players = players;
            for (Player player : players) {
                if (player instanceof Human) myHitpoint.setText("Мои жизни: " + String.valueOf(player.getHitpoint()));
                else botHitpoint.setText("Жизни бота: " + String.valueOf(player.getHitpoint()));
            }
        });
    }

    @Override
    public void showHand(List<Player> players, boolean isTurn) {
        SwingUtilities.invokeLater(() -> {
            for (Player player : players) {
                if (player instanceof Human) {
                    for (int i = 0; i < cards.size(); i++) {
                        if(i < player.getCardsOnHand().size()) {
                            cards.get(i)
                                 .setIcon(getScaledImageIconForCard(player.getCardsOnHand()
                                                                          .get(i)
                                                                          .getIcon()));
                        }
                        else cards.get(i).setIcon(getScaledImageIconForCard(noCard));
                    }
                }
            }
            if (isTurn){
                for (JLabel castLabel : castCards) castLabel.setIcon(null);
                finishButton.setEnabled(true);
            }
        });
    }

    @Override
    public void showWinner(Player player) {
        SwingUtilities.invokeLater(() -> {
            JOptionPane.showMessageDialog(this, "Победил " + player.getName() + "!", "А вот и победитель!!!", 1);
            setVisible(false);
            dispose();
            System.exit(0);
        });
    }

    @Override
    public void showRollDices(List<Integer> numerics, Player player) {
        SwingUtilities.invokeLater(() -> {
            setNullIconForDice();
            playerRollsDices.setText("Кидает: " + player.getName());
            String str = "<font face=\"Arial\" size=\"3\"><b>Кинул:</b> " + player.getName() + ". <b>Выпало:</b>";
            for (int i = 0; i < numerics.size(); i++) {
                JLabel label = labelsCubic.get(i);
                ImageIcon img;
                switch (numerics.get(i)) {
                    case 1:
                        img = new ImageIcon("images/dice/one.jpg");
                        str += " 1";
                        break;
                    case 2:
                        img = new ImageIcon("images/dice/two.jpg");
                        str += " 2";
                        break;
                    case 3:
                        img = new ImageIcon("images/dice/three.jpg");
                        str += " 3";
                        break;
                    case 4:
                        img = new ImageIcon("images/dice/four.jpg");
                        str += " 4";
                        break;
                    case 5:
                        img = new ImageIcon("images/dice/five.jpg");
                        str += " 5";
                        break;
                    case 6:
                        img = new ImageIcon("images/dice/six.jpg");
                        str += " 6";
                        break;
                    default:
                        img = new ImageIcon("images/dice/null.jpg");
                        break;
                }
                label.setIcon(getScaledImageIconForDice(img.getImage()));
            }
            logText.append(str + "<br></font>");
            log.setText(logText.toString());
        });
    }

    @Override
    public void showCast(Cast cast) {
        for (JLabel castLabel : castCards)
            castLabel.setIcon(null);
        castPlayer.setText("Кастует: " + cast.getPlayer().getName());
        String str = "<font face=\"Arial\" size=\"3\"><b>" + cast.getPlayer().getName() + " кастует заклинание:</b> ";

        for (int i = 0; i < cast.getCast().size(); i++) {
           JLabel label = castCards.get(i);
           label.setIcon(getScaledImageIconForCard(cast.getCast().get(i).getIcon()));

           str += " " + cast.getCast().get(i).getName();
        }
        logText.append(str + "<br></font>");
        log.setText(logText.toString());
        showHand(players, false);
    }

    @Override
    public void showChanges(List<Player> players) {
        SwingUtilities.invokeLater(() -> showHitpoint(players));
    }

    /**
     * @param image картинка карты
     * @return возвращает иконки нужного размера для карт
     */
    public static ImageIcon getScaledImageIconForCard(Image image) {
        return new ImageIcon(image.getScaledInstance(175, 235, Image.SCALE_DEFAULT));
    }

    /**
     * @param image картинка кубика
     * @return возвращает иконки нужного размера для кубиков
     */
    public static ImageIcon getScaledImageIconForDice(Image image) {
        return new ImageIcon(image.getScaledInstance(50, 50, Image.SCALE_DEFAULT));
    }

    /**
     * @param image картинка артефакта
     * @return возвращает иконки нужного размера для артефактов
     */
    public static ImageIcon getScaledImageIconForArtifact(Image image) {
        return new ImageIcon(image.getScaledInstance(90, 70, Image.SCALE_DEFAULT));
    }


    /**
     * устанавливает нулевые картинки кубиков нужного размера для всех лейбов
     */
    private void setNullIconForDice() {
        for (JLabel label : labelsCubic) {
            label.setIcon(getScaledImageIconForDice((new ImageIcon("images/dice/null.jpg").getImage())));
        }
    }

    //вызывается извне
    @Override
    public void pause() {
        SwingUtilities.invokeLater(() -> nextButton.setEnabled(true));
    }

    @Override
    public void selectCardInCast(int n) {
        castCards.get(n).setBorder(castBorder);
//        String str = "<font face=\"Arial\" size=\"3\"><b>Разыгрывается карта:</b> " +   + "<br>";
//        str += "<b>Описание:</b> " ;
//            logText.append(str + "<br></font>");
//        log.setText(logText.toString());
    }

    @Override
    public void deselectCardsInCast() {
        for(JLabel castCard : castCards){
            castCard.setBorder(null);
        }
    }

    @Override
    public void showText(String text) {
        logText.append(text).append("<br/>");
        log.setText(logText.toString());
    }

    @Override
    public void showArtifacts(Map<Player, List<AbstractArtifact>> artifactsByPlayer) {
        activeArtifactPanel.removeAll();
        passiveArtifactPanel.removeAll();
        for(Map.Entry<Player, List<AbstractArtifact>> entry : artifactsByPlayer.entrySet()){
            if(entry.getKey() instanceof Human){
                for(AbstractArtifact artifact : entry.getValue()){
                    if(artifact.getArtifactTypes().contains(ArtifactType.ACTIVE)){
                        JButton jButton = new JButton();
                        jButton.setToolTipText(getArtifactDescription(artifact));
                        jButton.setIcon(getScaledImageIconForArtifact(artifact.getIcon()));
                        jButton.addActionListener(e -> ((ActiveArtifact) artifact).activeArtifactAction());
                        activeArtifactPanel.add(jButton);
                        activeArtifactPanel.add(Box.createRigidArea(new Dimension(5,0)));
                    } else {
                        JLabel jLabel = new JLabel();
                        jLabel.setToolTipText(getArtifactDescription(artifact));
                        jLabel.setIcon(getScaledImageIconForArtifact(artifact.getIcon()));
                        passiveArtifactPanel.add(jLabel);
                        passiveArtifactPanel.add(Box.createRigidArea(new Dimension(5,0)));
                    }
                }
                break;
            }
        }
        revalidate();
        repaint();
        enemiesArtifactsFrame.showArtifacts(artifactsByPlayer);
    }

    public static String getArtifactDescription(AbstractArtifact artifact){
        return new StringBuilder("<html><b>").append(artifact.getName()).append("</b><br/>")
                                                 .append(artifact.getDescription()).append("</html>").toString();
    }
}

