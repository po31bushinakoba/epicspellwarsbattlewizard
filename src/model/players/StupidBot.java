package model.players;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс глупого бота. Рандомно кидает карты. Он будет играть с нами, а ИИ будет следать за нашей игрой и учиться
 */
public class StupidBot extends Player {

    public StupidBot() {
        setName("Глупый бот");
    }

    public StupidBot(String name){
        setName(name);
    }

    @Override
    public void createCast() {
        for(CardCastType cardType : CardCastType.values()){
            List<CastCard> cards = getCardsByType(cardType);
            if(cards.size() == 0) continue;
            addCard(cards.get(Game.getRandom().nextInt(cards.size())));
        }
    }

    /**
     * Получить карты с руки соотвентствующие определенному типу
     * @param cardType тип карты
     * @return
     */
    private List<CastCard> getCardsByType(CardCastType cardType){
        return getCardsOnHand().stream()
                               .filter(card -> card.getCardType()
                                                   .equals(cardType))
                               .collect(Collectors.toList());
    }
}
