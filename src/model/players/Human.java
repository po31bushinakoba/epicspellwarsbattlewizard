package model.players;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * Класс игрока живого человка
 */
public class Human extends Player {

    private volatile CountDownLatch lock;

    public Human() {
        setName("Живой человек");
        setCardsOnHand(new CopyOnWriteArrayList<>());
    }

    @Override
    public void createCast() {
        lock = new CountDownLatch(1);
        try {
            lock.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод показывает что игрок собрал заклинание. Вызывается извне
     */
    public void finish(){
        lock.countDown();
    }
}
