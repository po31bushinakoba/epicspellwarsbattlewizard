package model.players;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.Cast;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс бота тренера. Запоминает свои ходы в игре и если он выиграл в партии, то выгружаем мх в файл уроков
 */
public class Trainer extends StupidBot {

    private List<int[]> hits = new ArrayList<>();
    private List<float[]> params = new ArrayList<>();

    public Trainer() {
        setName("Учитель");
    }

    public Trainer(String name) {
        setName(name);
    }

    @Override
    public Cast turn() {
        Cast cast = super.turn();
        int myHitpoint = 0;
        int botHitpoint = 0;
        List<Player> players = Game.getPlayers();
        for(Player player : players){
            if(player.equals(this)) myHitpoint = player.getHitpoint();
            else botHitpoint = player.getHitpoint();
        }
        hits.add(new int[]{myHitpoint, botHitpoint});
        float[] cardsInTurn = new float[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for(CastCard card : cast.getCast()) cardsInTurn[card.getId()] = 1;
        params.add(cardsInTurn);

        return cast;
    }

    /**
     * Сохраняет успешный урок в файл уроков
     */
    public void saveInFile(){
        System.out.println(hits.size());
        try {
            File file = new File("nn");
            if(!file.exists()) file.mkdir();
            new File("nn/lessons").createNewFile();
            BufferedWriter bw = new BufferedWriter(new FileWriter("nn/lessons", true));
            for(int i = 0; i < hits.size(); i++){
                bw.write(hits.get(i)[0] + " " + hits.get(i)[1]);
                bw.newLine();
                bw.write(params.get(i)[0] + " " + params.get(i)[1] + " " + params.get(i)[2] + " " + params.get(i)[3] + " " +
                         params.get(i)[4] + " " + params.get(i)[5] + " " + params.get(i)[6] + " " + params.get(i)[7] + " " +
                         params.get(i)[8] + " " + params.get(i)[9] + " " + params.get(i)[10] + " " + params.get(i)[11] + " " +
                         params.get(i)[12] + " " + params.get(i)[13] + " " + params.get(i)[14] + " " + params.get(i)[15]);
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearLessons(){
        hits.clear();
        params.clear();
    }
}
