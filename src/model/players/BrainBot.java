package model.players;

import controller.Game;
import model.cards.cast.CastCard;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.io.IOException;
import java.util.*;

/**
 * Класс умного бота основанного на НС. Получает желательные параметры каста от НС, и затем ищет самое подходящие
 * заклинание, которое сможет составить из карты на руке (заклинание с меньшим штрафом)
 */
public class BrainBot extends Player {

    private MultiLayerNetwork net;

    public BrainBot() {
        setName("Умный бот");
        try {
            net = ModelSerializer.restoreMultiLayerNetwork("nn/net.zip");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void createCast() {
        int myHitpoint = 0;
        int botHitpoint = 0;
        for(Player player : Game.getPlayers()){
            if(player.equals(this)) myHitpoint = player.getHitpoint();
            else botHitpoint = player.getHitpoint();
        }

        //желательный карты для данной ситуации полученные от нейронной сети
        INDArray desiredCard = net.output(Nd4j.create(new double[]{myHitpoint, botHitpoint}, new int[]{1, 2}), false);
        Map<Integer, Float> map = new HashMap<>();
        for(int i = 0; i < 16; i++){
            map.put(i, desiredCard.getFloat(i));
        }
        List<Map.Entry<Integer, Float>> list = new ArrayList<>(map.entrySet());
        Collections.sort(list, (o1, o2) -> -o1.getValue().compareTo(o2.getValue()));
        for(Map.Entry<Integer, Float> entry : list){
            for(CastCard card : getCardsOnHand()){
                if(card.getId() == entry.getKey()) getCast().addCard(card);
            }
        }

    }
}
