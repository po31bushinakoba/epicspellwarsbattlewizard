package model.players;

import model.cards.artifact.ArtifactUtil;
import model.cards.artifact.tryaddcardincast.TryAddCardInCastArtifactStorage;
import model.cards.cast.CastCard;
import model.cards.cast.Cast;

import java.util.ArrayList;
import java.util.List;

import static model.cards.artifact.ArtifactType.TRY_ADD_CARD_IN_CAST;

/**
 * Абстрактный класс игрока
 */
public abstract class Player {

    private String name;
    private int hitpoint = 20;
    private List<CastCard> cardsOnHand = new ArrayList<>();
    private Cast cast = new Cast(this);
    private volatile boolean isTurn = false; //TODO atomic?


    /**
     * Игрок ходит
     * @return составленное заклинание
     */
    public Cast turn(){
        isTurn = true;
        createCast();
        isTurn = false;
        return getCast();
    }

    /**
     * Игрок составляет заклинание. Может выкинуть и пустое заклинание, что не хорошо. Нужно подумать как ограничить
     * @return составленное заклинание
     */
    protected abstract void createCast();



    public int getHitpoint() {
        return hitpoint;
    }

    public void setHitpoint(int hitpoint) {
        this.hitpoint = hitpoint;
    }

    public List<CastCard> getCardsOnHand() {
        return cardsOnHand;
    }

    public void setCast(Cast cast) {
        this.cast = cast;
    }

    public Cast getCast() {
        return cast;
    }

    public void setCardsOnHand(List<CastCard> cardsOnHand) {
        this.cardsOnHand = cardsOnHand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Добавляет карту к своему заклинанию
     * @param card добавляемая карта
     * @return удалось ли это сделать. если заклинание пытаются составить не по правилам, то запрещаем
     */
    public boolean addCard(CastCard card){
        if(!cardsOnHand.contains(card)) return false; //проверка на то есть ли вообще такая карта на руке. Мало ли?
        ((TryAddCardInCastArtifactStorage) ArtifactUtil.getArtifactsByType(TRY_ADD_CARD_IN_CAST)).callArtifactsAction(card, this);
        if(!isTurn) return false;
        boolean isAdd = cast.addCard(card);
        return isAdd;
    }
    /**
     * Удаляет карту из своего заклинания
     * @param card удаляемая карта
     * @return удалось ли это сделать
     */
    public boolean removeCard(CastCard card){
        if(!isTurn) return false;
        boolean isRemove = cast.removeCard(card);
        return isRemove;
    }

    /**
     * Добавляем (или убавляем, если с минусом) хитпоинты игрока
     * @param count
     */
    public void addHitpoint(int count){
        hitpoint += count;
    }

    @Override
    public String toString() {
        return name;
    }
}
