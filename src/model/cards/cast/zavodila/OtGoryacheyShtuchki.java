package model.cards.cast.zavodila;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class OtGoryacheyShtuchki extends CastCard {

    public OtGoryacheyShtuchki() {
        try {
            setIcon(ImageIO.read(new File("images/otgoryacheyshtuchki.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("От Жарёхи");
        setDescription("Нанеси 3 урона самому живучему врагу.");
        setCardType(CardCastType.ZAVODILA);
        setSchoolMagic(SchoolMagic.UGAR);
        setIniciative(0);
        setId(4);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        Game.mostAliveEnemies(cast.getPlayer()).forEach((player) -> player.addHitpoint(-3));
    }
}
