package model.cards.cast.zavodila;

import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class OtBradostrela extends CastCard {

    public OtBradostrela()  {
        try {
            setIcon(ImageIO.read(new File("images/otbradostrela.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("От Ветро-борода");
        setDescription("Эта карта копирует текст прихода из твоего заклинания.");
        setCardType(CardCastType.ZAVODILA);
        setSchoolMagic(SchoolMagic.PORCHA);
        setIniciative(0);
        setId(1);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        for(int i = 0; i < cast.getCast().size(); i++){
            if(cast.getCast().get(i).getCardType().equals(CardCastType.PRIKHOD)) cast.getCast().get(i).action(cast, players);
        }
    }
}
