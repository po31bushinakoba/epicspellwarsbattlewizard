package model.cards.cast.zavodila;


import controller.Game;
import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactUtil;
import model.cards.cast.*;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class OtBenaVudu extends CastCardWithSelect {

    public OtBenaVudu() {
        try {
            setIcon(ImageIO.read(new File("images/otbenavudu.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("От Вуду Бена");
        setDescription("Каждый враг бросает 1 кубик и отхватывает столько урона, сколько выпало. Сбрось 1 своё сокровище (если есть).");
        setCardType(CardCastType.ZAVODILA);
        setSchoolMagic(SchoolMagic.MRAK);
        setIniciative(0);
        setId(0);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        for(Player player : players){
            if(!player.equals(cast.getPlayer())){
                int cubicResult = Game.rollDice(player);
                player.addHitpoint(-cubicResult);
            }
        }
        if(!ArtifactUtil.getArtifactsByPlayer(cast.getPlayer()).isEmpty()){
            ArtifactUtil.resetArtifact(cast.getPlayer(),
                                       (AbstractArtifact) select(cast.getPlayer(),
                                                                 "Выбери сокровище которое будешь скидывать.",
                                                                 ArtifactUtil.getArtifactsByPlayer(cast.getPlayer())));
        }
    }
}
