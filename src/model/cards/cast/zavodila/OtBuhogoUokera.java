package model.cards.cast.zavodila;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class OtBuhogoUokera extends CastCard {

    public OtBuhogoUokera()  {
        try {
            setIcon(ImageIO.read(new File("images/otbuhogouokera.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("От Хроно Йокера");
        setDescription("Каждый игрок бросает кубик. Прибавь к своему результату 1 за каждый уникальный знак в твоём заклинании. Игрок(и) с наименьшим результатом отхватывает(ют) 3 урона.");
        setCardType(CardCastType.ZAVODILA);
        setSchoolMagic(SchoolMagic.KUMAR);
        setIniciative(0);
        setId(2);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        int minResult = 7;
        List<Player> playersWithMinResult = new ArrayList<>();
        for(Player player : players){
            int cubicResult = Game.rollDice(player) + (cast.getPlayer().equals(player) ? cast.getUniqueSchoolInCast() : 0);
            if(cubicResult == minResult) playersWithMinResult.add(player);
            else if(cubicResult < minResult){
                playersWithMinResult.clear();
                playersWithMinResult.add(player);
                minResult = cubicResult;
            }
        }
        playersWithMinResult.forEach((player) -> player.addHitpoint(-3));
    }
}
