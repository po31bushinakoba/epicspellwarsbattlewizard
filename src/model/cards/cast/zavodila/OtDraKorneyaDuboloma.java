package model.cards.cast.zavodila;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class OtDraKorneyaDuboloma extends CastCard {

    public OtDraKorneyaDuboloma()  {
        try {
            setIcon(ImageIO.read(new File("images/otdrakorneyaduboloma.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("От Дока Корнедрева");
        setDescription("Накрути 3 жизни. Каждый враг бросает кубик: те, у кого выпало 6, накручивают по 3 жизни.");
        setCardType(CardCastType.ZAVODILA);
        setSchoolMagic(SchoolMagic.TRAVA);
        setIniciative(0);
        setId(3);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        cast.getPlayer().addHitpoint(3);
        for(Player player : players){
            if(!player.equals(cast.getPlayer()) && Game.rollDice(player) == 6) player.addHitpoint(3);
        }
    }
}
