package model.cards.cast.navorot;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class Gromoboyniy extends CastCard {

    public Gromoboyniy() {
        try {
            setIcon(ImageIO.read(new File("images/gromoboyniy.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Оглушающий");
        setDescription("Нанеси 2 урона случайному врагу. Примени этот эффект один раз за каждый уникальный знак в твоём заклинании.");
        setCardType(CardCastType.NAVOROT);
        setSchoolMagic(SchoolMagic.TRAVA);
        setIniciative(0);
        setId(9);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        List<Player> playersWithoutMe = new ArrayList<>(players);
        playersWithoutMe.remove(cast.getPlayer());
        for(int i = 0; i < cast.getUniqueSchoolInCast(); i++){
            playersWithoutMe.get(Game.rollDice(cast.getPlayer()) % playersWithoutMe.size()).addHitpoint(-2);
        }
    }
}
