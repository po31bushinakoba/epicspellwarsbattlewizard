package model.cards.cast.navorot;

import model.cards.artifact.ArtifactUtil;
import model.cards.cast.*;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class Dvulichniy extends CastCardWithSelect {

    public Dvulichniy()  {
        try {
            setIcon(ImageIO.read(new File("images/dvulichniy.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Дву-личный");
        setDescription("Колдун по твоему выбору берёт 1 сокровище. Затем нанеси ему 2 урона за каждое его сокровище.");
        setCardType(CardCastType.NAVOROT);
        setSchoolMagic(SchoolMagic.PORCHA);
        setIniciative(0);
        setId(6);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        Player target = (Player) select(cast.getPlayer(), "Выбери игрока, по которому хочешь провести атаку. ", players);
        ArtifactUtil.takeArtifact(target);
        target.addHitpoint(- ArtifactUtil.getArtifactsByPlayer(target).size() * 2);
    }
}
