package model.cards.cast.navorot;

import model.cards.cast.CardCastType;
import model.cards.cast.CastCardWithSelect;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class Diskotechniy extends CastCardWithSelect {

    public Diskotechniy() {
        try {
            setIcon(ImageIO.read(new File("images/dickotechniy.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Диско-шарный");
        setDescription("Эта карта копирует текст заводилы или прихода из твоего заклинания.");
        setCardType(CardCastType.NAVOROT);
        setSchoolMagic(SchoolMagic.KUMAR);
        setIniciative(0);
        setId(7);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        CardCastType cardType = (CardCastType) select(cast.getPlayer(), "Что будем копировать? ", Arrays.asList(CardCastType.ZAVODILA, CardCastType.PRIKHOD));
        for(int i = 0; i < cast.getCast().size(); i++){
            if(cast.getCast().get(i).getCardType().equals(cardType)) cast.getCast().get(i).action(cast, players);
        }
    }
}
