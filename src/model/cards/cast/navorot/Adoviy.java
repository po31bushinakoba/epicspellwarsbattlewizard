package model.cards.cast.navorot;

import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 11.02.2017.
 */
public class Adoviy extends CastCard {

    public Adoviy()  {
        try {
            setIcon(ImageIO.read(new File("images/adoviy.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Адско-стический");
        setDescription("Нанеси каждому врагу 1 урон за каждый знак угара в твоём заклинании.");
        setCardType(CardCastType.NAVOROT);
        setSchoolMagic(SchoolMagic.UGAR);
        setIniciative(0);
        setId(5);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        int ugarCount = cast.getCountSchoolMagicByTypeInCast(SchoolMagic.UGAR);
        for(Player player : players){
            if(!player.equals(cast.getPlayer())){
                player.addHitpoint(-ugarCount);
            }
        }
    }
}
