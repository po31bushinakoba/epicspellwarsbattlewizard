package model.cards.cast.navorot;

import controller.Game;
import model.cards.cast.CardCastType;
import model.cards.cast.CastCardWithSelectTarget;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class Dyavolskiy extends CastCardWithSelectTarget {

    public Dyavolskiy()  {
        try {
            setIcon(ImageIO.read(new File("images/dyavolskiy.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Дуэльадский");
        setDescription("Жертва: враг по твоему выбору.\nМогучий бросок:\n1-4 2 урона.\n5-9 4 урона, а ты отхватываешь 1 урон.\n10+ 5 уронов, а ты отхватываешь 2 урона.");
        setCardType(CardCastType.NAVOROT);
        setSchoolMagic(SchoolMagic.MRAK);
        setIniciative(0);
        setId(8);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        Player target = selectTarget(cast.getPlayer(), players);
        int powerfullRollDicesResult = Game.powerfullRollDices(cast, getSchoolMagic());
        if(powerfullRollDicesResult <= 4) target.addHitpoint(-2);
        else if(powerfullRollDicesResult <= 9){
            target.addHitpoint(-4);
            cast.getPlayer().addHitpoint(-1);
        }
        else{
            target.addHitpoint(-5);
            cast.getPlayer().addHitpoint(-2);
        }
    }
}
