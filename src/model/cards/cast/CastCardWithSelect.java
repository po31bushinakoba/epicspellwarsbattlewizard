package model.cards.cast;

import controller.Game;
import model.players.Human;
import model.players.Player;
import view.AbstractPlayingFieldWithHuman;

import java.util.List;

/**
 * Класс карт в которых игрок может сам выбрать что-либо
 */
abstract public class CastCardWithSelect extends CastCard {

    /**
     * Выбор чего-либо
     * @param player кто разыгрывает
     * @param text текст для отображения
     * @param choices из чего выбираем
     * @return что-либо
     */
    protected Object select(Player player, String text, List choices){
        if(player instanceof Human){
            StringBuilder textBuilder = new StringBuilder("<html>").append(text).append("<br/>");
            for(int i = 0; i < choices.size(); i++){
                textBuilder.append(choices.get(i).toString()).append(" (").append(i).append(")<br/>");
            }
            textBuilder.append("</html>");

            return choices.get(((AbstractPlayingFieldWithHuman) Game.getPlayingField())
                                                                    .showHumanChoice(textBuilder.toString(), choices.size()));

        }
        else return choices.get(Game.getRandom().nextInt(choices.size()));
    }
}
