package model.cards.cast.prikhod;

import controller.Game;
import model.cards.artifact.ArtifactUtil;
import model.cards.cast.*;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class VihrBodrosti extends CastCardWithSelect {

    public VihrBodrosti()  {
        try {
            setIcon(ImageIO.read(new File("images/vihrbodrosti.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Вихрь Силы");
        setDescription("Жертвы: каждый враг.\nМогучий бросок:\n1-4 Ты сбрасываешь 1 карту.\n5-9 2 урона, а ты сбрасываешь 2 карты.\n10+ То же, что и выше, но ты ещё берёшь 1 сокровище.");
        setCardType(CardCastType.PRIKHOD);
        setSchoolMagic(SchoolMagic.PORCHA);
        setIniciative(16);
        setId(10);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        int powerfullRollDicesResult = Game.powerfullRollDices(cast, getSchoolMagic());
        if(powerfullRollDicesResult <= 4){
            removeCardOnPlayerHand(cast.getPlayer());
            return;
        }
        removeCardOnPlayerHand(cast.getPlayer());
        removeCardOnPlayerHand(cast.getPlayer());
        for(Player player : players){
            if(!player.equals(cast.getPlayer())) player.addHitpoint(-2);
        }
        if(powerfullRollDicesResult >= 10) ArtifactUtil.takeArtifact(cast.getPlayer());
    }

    /**
     * Сбрасываем карту с руки
     * @param player
     * @return
     */
    private void removeCardOnPlayerHand(Player player){
        List<CastCard> cardsOnHand = new ArrayList<>(player.getCardsOnHand());
        cardsOnHand.removeAll(player.getCast().getCast()); // оставляем только те карты, что реально остались на руке
        if(cardsOnHand.isEmpty()) return;
        player.getCardsOnHand().remove(select(player, "Что будем сбрасывать?", cardsOnHand));
    }
}
