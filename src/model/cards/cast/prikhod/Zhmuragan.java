package model.cards.cast.prikhod;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class Zhmuragan extends CastCard {

    public Zhmuragan()  {
        try {
            setIcon(ImageIO.read(new File("images/zmuragan.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Расчленёнка");
        setDescription("Жертва: самый живучий враг.\nМогучий бросок:\n1-4 2 урона.\n5-9 3 урона.\n10+ 6 уронов.");
        setCardType(CardCastType.PRIKHOD);
        setSchoolMagic(SchoolMagic.TRAVA);
        setIniciative(2);
        setId(12);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        int powerfullRollDicesResult = Game.powerfullRollDices(cast, getSchoolMagic());
        for(Player player : Game.mostAliveEnemies(cast.getPlayer())) {
            if (powerfullRollDicesResult <= 4) player.addHitpoint(-2);
            else if (powerfullRollDicesResult <= 9)  player.addHitpoint(-3);
            else player.addHitpoint(-6);
        }
    }
}
