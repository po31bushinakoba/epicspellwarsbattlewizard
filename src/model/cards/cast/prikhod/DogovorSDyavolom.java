package model.cards.cast.prikhod;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class DogovorSDyavolom extends CastCard {

    public DogovorSDyavolom()  {
        try {
            setIcon(ImageIO.read(new File("images/dogovorsdyavolom.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Сделка с дьяволом");
        setDescription("Жертва: самый живучий враг.\nМогучий бросок:\n1-4 1 урон.\n5-9 2 урона\n10+ 2 урона, а если в заклинании жертвы есть приход, отожми его и добавь к своему заклинанию.");
        setCardType(CardCastType.PRIKHOD);
        setSchoolMagic(SchoolMagic.MRAK);
        setIniciative(18);
        setId(11);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        int powerfullRollDicesResult = Game.powerfullRollDices(cast, getSchoolMagic());
        for(Player player : Game.mostAliveEnemies(cast.getPlayer())){
            if(powerfullRollDicesResult <= 4) player.addHitpoint(-1);
            else if(powerfullRollDicesResult <= 9) player.addHitpoint(-2);
            else {
                player.addHitpoint(-2);
                List<CastCard> cardsOtherPlayer = new ArrayList<>(player.getCast().getCast());
                for(int i = 0; i < cardsOtherPlayer.size(); i++){
                    if(CardCastType.PRIKHOD.equals(cardsOtherPlayer.get(i).getCardType())){
                        cast.getCast().add(cardsOtherPlayer.get(i));
                        player.getCast().getCast().remove(cardsOtherPlayer.get(i));
                    }
                }
            }
        }
        Game.getPlayingField().showCast(cast);
    }
}
