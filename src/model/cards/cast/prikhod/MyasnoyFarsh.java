package model.cards.cast.prikhod;

import controller.Game;
import model.cards.cast.CastCard;
import model.cards.cast.CardCastType;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class MyasnoyFarsh extends CastCard {

    public MyasnoyFarsh()  {
        try {
            setIcon(ImageIO.read(new File("images/myasnoyfarsh.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Мясокуча");
        setDescription("Жертвы: каждый враг живучее тебя.\nМогучий бросок:\n1-4 1 урон.\n5-9 3 урона.\n10+ 4 урона.");
        setCardType(CardCastType.PRIKHOD);
        setSchoolMagic(SchoolMagic.UGAR);
        setIniciative(20);
        setId(13);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        int powerfullRollDicesResult = Game.powerfullRollDices(cast, getSchoolMagic());
        for(Player player : Game.alivestMeEnemies(cast.getPlayer())) {
            if (powerfullRollDicesResult <= 4) player.addHitpoint(-1);
            else if (powerfullRollDicesResult <= 9)  player.addHitpoint(-3);
            else player.addHitpoint(-4);
        }
    }
}
