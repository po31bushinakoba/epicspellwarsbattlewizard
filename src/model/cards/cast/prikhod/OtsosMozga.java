package model.cards.cast.prikhod;

import controller.Game;
import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactUtil;
import model.cards.cast.CardCastType;
import model.cards.cast.CastCardWithSelectTarget;
import model.cards.cast.Cast;
import model.cards.cast.SchoolMagic;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class OtsosMozga extends CastCardWithSelectTarget {

    public OtsosMozga()  {
        try {
            setIcon(ImageIO.read(new File("images/otsosmozga.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Мозгосос");
        setDescription("Жертва: враг по твоему выбору.\nМогучий бросок:\n1-4 1 урон.\n5-9 3 урона.\n10+ 4 урона, а ты можешь отжать у жертвы 1 сокровище по твоему выбору.");
        setCardType(CardCastType.PRIKHOD);
        setSchoolMagic(SchoolMagic.KUMAR);
        setIniciative(15);
        setId(14);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        Player target = selectTarget(cast.getPlayer(), players);
        int powerfullRollDicesResult = Game.powerfullRollDices(cast, getSchoolMagic());
        if(powerfullRollDicesResult <= 4) target.addHitpoint(-1);
        else if(powerfullRollDicesResult <= 9) target.addHitpoint(-3);
        else{
            target.addHitpoint(-4);
            if(!ArtifactUtil.getArtifactsByPlayer(target).isEmpty()){
                ArtifactUtil.takeArtifactOtherPlayer(cast.getPlayer(), target,
                                                     (AbstractArtifact) select(cast.getPlayer(),
                                                                               "Выбери сокровище которое хочешь отжать.",
                                                                               ArtifactUtil.getArtifactsByPlayer(target)));
            }
        }
    }
}
