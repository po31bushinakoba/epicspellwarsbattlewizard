package model.cards.cast;

import controller.Game;
import model.cards.cast.navorot.*;
import model.cards.cast.prikhod.*;
import model.cards.cast.shalnayamagiya.ShalnayaMagiya;
import model.cards.cast.zavodila.*;
import model.players.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Класс агрегирующий работу с картами хаклинаний
 */
public class CardCastUtil {

    private static List<CastCard> deck = new ArrayList<>();
    //все карты участвующие в игре
    private static List<CastCard> allCards = Arrays.asList(//заводилы
                                                           new OtBenaVudu(), new OtBenaVudu(),
                                                           new OtBradostrela(), new OtBradostrela(),
                                                           new OtBuhogoUokera(), new OtBuhogoUokera(),
                                                           new OtDraKorneyaDuboloma(), new OtDraKorneyaDuboloma(),
                                                           new OtGoryacheyShtuchki(), new OtGoryacheyShtuchki(),
                                                           //навороты
                                                           new Adoviy(), new Adoviy(),
                                                           new Diskotechniy(), new Diskotechniy(),
                                                           new Dvulichniy(), new Dvulichniy(),
                                                           new Dyavolskiy(), new Dyavolskiy(),
                                                           new Gromoboyniy(), new Gromoboyniy(),
                                                           //приходы
                                                           new DogovorSDyavolom(), new DogovorSDyavolom(),
                                                           new MyasnoyFarsh(), new MyasnoyFarsh(),
                                                           new OtsosMozga(), new OtsosMozga(),
                                                           new VihrBodrosti(), new VihrBodrosti(),
                                                           new Zhmuragan(), new Zhmuragan(),
                                                           //шальная магия
                                                           new ShalnayaMagiya(), new ShalnayaMagiya()
                                                          );

    static {
        createDeck();
    }

    /**
     * Раздаём карты
     */
    public static void dealCards(){
        for (Player player : Game.getPlayers()){
            player.getCardsOnHand().addAll(takeCardsFromDeck(8 - player.getCardsOnHand().size()));
        }

    }

    /**
     * Создаём колоду
     */
    public static void createDeck(){
        deck.clear();
        deck.addAll(allCards);
        //удаляем те карты которые уже есть на руках у игроков
        for(Player player : Game.getPlayers()){
            for(CastCard card : player.getCardsOnHand()){
                deck.remove(card);
            }
        }
        //мешаем колоду
        Collections.shuffle(deck, Game.getRandom());
    }

    /**
     * Берем карты из колоды
     * @param n сколько карты берем
     * @return список карты
     */
    public static List<CastCard> takeCardsFromDeck(int n){
        List<CastCard> cards = new ArrayList<>();
        for(int i = 0; i < n ; i++){
            if(deck.size() == 0) createDeck();
            cards.add(deck.get(0));
            deck.remove(0);
        }
        return cards;
    }

    /**
     * Берем карты из колоды пока не встретим с нужным типом
     * @param  cardType с каким типом карту ищем
     * @return нужная карта
     */
    public static CastCard takeCardFromDeckByType(CardCastType cardType){
        CastCard card;
        do{
            card = takeCardsFromDeck(1).get(0);
        } while (!cardType.equals(card.getCardType()));
        return card;
    }

    /**
     * Возращает карту из листа по её типу
     * @param cards лист карт
     * @return карту или null если такой карты нет
     */
    public static List<CastCard> findCardsByType(List<CastCard> cards, CardCastType cardType){
        List<CastCard> cardByType = new ArrayList<>();
        for(CastCard card : cards){
            if(cardType.equals(card.getCardType())) cardByType.add(card);
        }
        return cardByType;
    }
}
