package model.cards.cast;

/**
 * Тип карты
 */
public enum CardCastType {

    ZAVODILA(0, "Заводила"), NAVOROT(1, "Наворот"), PRIKHOD(2, "Приход"), SHALNAYA_MAGIYA(-1, "Шальная магия");

    private int order;
    private String name;

    public int getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    CardCastType(int order, String name) {
        this.order = order;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
