package model.cards.cast;

/**
 * Школы магии
 */
public enum SchoolMagic {
    PORCHA, MRAK, UGAR, KUMAR, TRAVA
}
