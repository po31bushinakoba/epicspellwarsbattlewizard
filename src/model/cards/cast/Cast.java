package model.cards.cast;

import controller.Game;
import model.cards.artifact.ArtifactUtil;
import model.cards.artifact.aftercountschoolmagic.AfterCountSchoolMagicArtifactStorage;
import model.players.Player;

import java.util.*;
import java.util.stream.Collectors;

import static model.cards.artifact.ArtifactType.AFTER_COUNT_SCHOOL_MAGIC;

/**
 * Класс заклинания
 */
public class Cast {

    private List<CastCard> cast = new ArrayList<>();
    private Player player;
    private int initiative = 0;

    public Cast(Player player) {
        this.player = player;
    }

    /**
     * Удаляет карту из заклинания
     * @param card удаляемая карта
     * @return удалось ли это сделать
     */
    public boolean removeCard(CastCard card){
        boolean isRemove =  cast.remove(card);
        if(isRemove && CardCastType.PRIKHOD.equals(card.getCardType())) initiative = 0;
        return isRemove;
    }

    /**
     * Добавляет карту к заклинанию
     * @param card добавляемая карта
     * @return удалось ли это сделать. если заклинание пытаются составить не по правилам, то запрещаем
     */
    public boolean addCard(CastCard card){
        if(cast.contains(card)) return false;
        if(cast.size() >= 3) return false;
        for(CastCard cardInCast : cast){
            if(!cardInCast.getCardType().equals(CardCastType.SHALNAYA_MAGIYA) &&
               cardInCast.getCardType().equals(card.getCardType())) return false;
        }
        if(CardCastType.PRIKHOD.equals(card.getCardType())) initiative = card.getIniciative();
        cast.add(card);
        return true;
    }

    /**
     * Очищает заклинание
     */
    public void clearCast(){
        cast.clear();
        initiative = 0;
    }

    /**
     * Разыграть заклинание. Убрать из него все карты после разыгрывания
     * @param players все игроки в игре
     */
    public void play(List<Player> players){
        player.getCardsOnHand().removeAll(cast);
        Collections.sort(cast, Comparator.comparingInt(card -> card.getCardType().getOrder()));
        Game.getPlayingField().showCast(this);
        for(int i = 0; i < cast.size(); i++){
            cast.get(i).play(this, players);
        }
        initiative = 0;
        cast.clear();
    }

    public int getInitiative() {
        return initiative;
    }

    /**
     * Возвращает количество карт принадлежащих к определенной школе в заклинании
     * @param schoolMagic школа магии
     * @return
     */
    public int getCountSchoolMagicByTypeInCast(SchoolMagic schoolMagic){
        int count = 0;
        for(CastCard card : getCast()){
            if(schoolMagic.equals(card.getSchoolMagic())) count++;
        }
        count += ((AfterCountSchoolMagicArtifactStorage) ArtifactUtil
                .getArtifactsByType(AFTER_COUNT_SCHOOL_MAGIC)).callArtifactsAction(count, schoolMagic);
        return count;
    }

    /**
     * Подсчитать количество уникальных знаков в заклинании
     * @return
     */
    public int getUniqueSchoolInCast(){
        return getCast().stream()
                        .filter(card -> !card.getCardType().equals(CardCastType.SHALNAYA_MAGIYA))
                        .map(CastCard::getSchoolMagic)
                        .collect(Collectors.toSet()).size();
    }

    public List<CastCard> getCast() {
        return cast;
    }

    public Player getPlayer() {
        return player;
    }
}
