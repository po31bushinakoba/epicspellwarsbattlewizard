package model.cards.cast.shalnayamagiya;

import controller.Game;
import model.cards.cast.*;
import model.players.Player;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Evgeniy on 12.02.2017.
 */
public class ShalnayaMagiya extends CastCardWithSelect {

    public ShalnayaMagiya()  {
        try {
            setIcon(ImageIO.read(new File("images/shalnayamagiya.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Дикая магия");
        setDescription("Используй вместо любой другой карты в заклинании. Перед тем как применять эффекты заклинания, открывай с верха колоды заклинаний, пока не найдешь карту того типа, который заменил \"Шальной магией\". Выложи эту карту вместо \"Шальной магии\", а саму \"Шальную магию\" сбрось вместе с другими открывшимися картами из колоды.");
        setCardType(CardCastType.SHALNAYA_MAGIYA);
        setSchoolMagic(null);
        setIniciative(0);
        setId(15);
    }

    @Override
    public void action(Cast cast, List<Player> players) {
        List<CardCastType> cardTypes = new ArrayList<>(Arrays.asList(CardCastType.values()));
        for(CastCard card : cast.getCast()){
            cardTypes.remove(card.getCardType());
        }

        CardCastType cardType = (CardCastType) select(cast.getPlayer(), "Что будем дотягивать? ", cardTypes);
        CastCard card = CardCastUtil.takeCardFromDeckByType(cardType);
        for(int i = cast.getCast().indexOf(this) + 1; ; i++){ //начинаем поиска место с текущей позиции, чтобы не вставить её в начало, где карты уже были разыграны
            if (i == cast.getCast().size()){
                //мы дошли до конца, вставляем вконец
                cast.getCast().add(card);
                break;
            } else if (card.getCardType().getOrder() < cast.getCast().get(i).getCardType().getOrder()) {
                //следущая карта выше нашей по порядку, то есть нам нужно вставить карту сюда
                cast.getCast().add(i, card);
                break;
            }
        }
        Game.getPlayingField().showCast(cast);
    }
}
