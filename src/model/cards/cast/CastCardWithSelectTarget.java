package model.cards.cast;

import model.players.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс карты в котором игрок выбирает жертву сам
 */
public abstract class CastCardWithSelectTarget extends CastCardWithSelect {

    /**
     * Выбор жертвы
     * @param player кто разыгрывает
     * @param players все игроки
     * @return жертва
     */
    protected Player selectTarget(Player player, List<Player> players){
        List<Player> playersWithoutMe = new ArrayList<>(players);
        playersWithoutMe.remove(player);
        return (Player) select(player, "Выбери игрока, по которому хочешь провести атаку. ", playersWithoutMe);
    }
}
