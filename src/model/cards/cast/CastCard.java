package model.cards.cast;

import controller.Game;
import model.cards.Card;
import model.players.Player;

import java.util.List;

/**
 * Класс карты заклинаний
 */
public abstract class CastCard extends Card {


    private CardCastType cardType;
    private SchoolMagic schoolMagic;
    private int iniciative;
    private int id;

    /**
     * Метод вызывается при разыгрывании карты
     * @param cast заклинание в котором находится эта карта
     * @param players все игроки в игре
     */
    public void play(Cast cast, List<Player> players){
        Game.getPlayingField().selectCardInCast(cast.getCast().indexOf(this));
        Game.pauseOn();
        action(cast, players);
        Game.getPlayingField().showHitpoint(players);
        Game.getPlayingField().deselectCardsInCast();
    }

    /**
     * Эффект карты
     * @param cast заклинание в котором находится эта карта
     * @param players все игроки в игре
     */
    abstract public void action(Cast cast, List<Player> players);

    public CardCastType getCardType() {
        return cardType;
    }

    public SchoolMagic getSchoolMagic() {
        return schoolMagic;
    }

    public int getIniciative() {
        return iniciative;
    }

    public void setCardType(CardCastType cardType) {
        this.cardType = cardType;
    }

    public void setSchoolMagic(SchoolMagic schoolMagic) {
        this.schoolMagic = schoolMagic;
    }

    public void setIniciative(int iniciative) {
        this.iniciative = iniciative;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new ClassCastException();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
