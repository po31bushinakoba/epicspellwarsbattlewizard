package model.cards.artifact;

import java.util.ArrayList;
import java.util.List;

/**
 * хранилище артефактов
 */
public class ArtifactStorage<T extends ArtifactCard> {

    protected List<T> artifacts = new ArrayList<>();

    public void add(T artifact){
        artifacts.add(artifact);
    }

    public boolean remove(T artifact){
        return artifacts.remove(artifact);
    }

    public List<T> getArtifacts(){
        return artifacts;
    }
}
