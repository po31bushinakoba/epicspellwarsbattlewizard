package model.cards.artifact.active;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;

import java.util.Arrays;
import java.util.List;

/**
 * Абстрактные класс активных артефактов
 */
public abstract class AbstractActiveArtifact extends AbstractArtifact implements ActiveArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.ACTIVE);

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }
}
