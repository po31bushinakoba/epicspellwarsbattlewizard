package model.cards.artifact.active;

import model.cards.artifact.ArtifactCard;

/**
 * Интерфейс для активных артефактов
 */
public interface ActiveArtifact extends ArtifactCard{

    void activeArtifactAction();
}
