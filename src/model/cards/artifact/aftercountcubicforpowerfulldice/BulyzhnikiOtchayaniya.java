package model.cards.artifact.aftercountcubicforpowerfulldice;

import controller.Game;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Evgeniy on 20.03.2017.
 */
public class BulyzhnikiOtchayaniya extends AbstractAfterCountCubicsForPowerfullDiceArtifact{

    public BulyzhnikiOtchayaniya()  {
        try {
            setIcon(ImageIO.read(new File("images/bulyzhnikiotchayaniya.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Булыжники отчаяния");
        setDescription("Если у тебя 9 или меньше жизней, добавляй 1 кубик к каждому своему могучему броску.");
    }

    @Override
    public int afterCountCubicsForPowerfullDiceArtifactAction(int result) {
        int deltaResult = 0;
        if(getOwner().getHitpoint() <= 9){
            Game.getPlayingField().showText("<9 жизней. Булыжники отчаяние добавляют 1 кубик!");
            return ++deltaResult;
        }
        else return deltaResult;
    }
}
