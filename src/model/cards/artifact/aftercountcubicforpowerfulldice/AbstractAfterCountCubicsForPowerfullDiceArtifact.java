package model.cards.artifact.aftercountcubicforpowerfulldice;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;

import java.util.Arrays;
import java.util.List;

/**
 * Абстрактный класс активных артефактов
 */
public abstract class AbstractAfterCountCubicsForPowerfullDiceArtifact extends AbstractArtifact implements AfterCountCubicsForPowerfullDiceArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.AFTER_CUBIC_COUNT_FOR_POWERFULL_DICE);

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }
}
