package model.cards.artifact.aftercountcubicforpowerfulldice;

import model.cards.artifact.ArtifactCard;

/**
 * Интерфейс для артефактов эффект которых вызывается после подсчета кубиков для мощного броска
 */
public interface AfterCountCubicsForPowerfullDiceArtifact extends ArtifactCard {

    int afterCountCubicsForPowerfullDiceArtifactAction(int result);
}
