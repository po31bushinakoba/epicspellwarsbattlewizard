package model.cards.artifact.aftercountcubicforpowerfulldice;

import controller.Game;
import model.cards.artifact.ArtifactStorage;

/**
 * Created by Evgeniy on 25.03.2017.
 */
public class AfterCountCubicForPowerfullDiceArtifactStorage extends ArtifactStorage<AfterCountCubicsForPowerfullDiceArtifact> {

    public int callArtifactsAction(int result){
        int deltaResult = 0;
        for(AfterCountCubicsForPowerfullDiceArtifact artifact : artifacts){
            if(artifact.getOwner() == Game.getLastTurnedPlayer()) {
                deltaResult += artifact.afterCountCubicsForPowerfullDiceArtifactAction(result);
            }
        }
        return deltaResult;
    }
}
