package model.cards.artifact.aftercountschoolmagic;

import controller.Game;
import model.cards.cast.SchoolMagic;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Evgeniy on 20.03.2017.
 */
public class Zhgufli extends AbstractAfterCountSchoolMagicArtifact {

    public Zhgufli()  {
        try {
            setIcon(ImageIO.read(new File("images/zhgufli.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Жгуфли");
        setDescription("Это сокровище считается картой со знаком угара в каждом твоём заклинании.");
    }

    @Override
    public int afterCountSchoolMagicArtifactAction(int result, SchoolMagic schoolMagic) {
        int deltaResult = 0;
        if(schoolMagic == SchoolMagic.UGAR){
            Game.getPlayingField().showText("Жгуфли добавляют 1 знак угара!");
            return ++deltaResult;
        }
        else return deltaResult;
    }
}
