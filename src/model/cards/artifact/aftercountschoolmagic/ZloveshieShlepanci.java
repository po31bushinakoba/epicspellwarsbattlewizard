package model.cards.artifact.aftercountschoolmagic;

import controller.Game;
import model.cards.cast.SchoolMagic;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Evgeniy on 20.03.2017.
 */
public class ZloveshieShlepanci extends AbstractAfterCountSchoolMagicArtifact {

    public ZloveshieShlepanci()  {
        try {
            setIcon(ImageIO.read(new File("images/zloveshieshlepanci.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Зловещие шлепанцы");
        setDescription("Это сокровище считается картой со знаком мрака в каждом твоём заклинании.");
    }

    @Override
    public int afterCountSchoolMagicArtifactAction(int result, SchoolMagic schoolMagic) {
        int deltaResult = 0;
        if(schoolMagic == SchoolMagic.MRAK){
            Game.getPlayingField().showText("Башмаки скоморах добавляют 1 знак мрака!");
            return ++deltaResult;
        }
        else return deltaResult;
    }
}
