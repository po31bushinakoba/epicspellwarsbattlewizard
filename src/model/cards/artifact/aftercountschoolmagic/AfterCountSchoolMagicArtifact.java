package model.cards.artifact.aftercountschoolmagic;

import model.cards.artifact.ArtifactCard;
import model.cards.cast.SchoolMagic;

/**
 * Интерфейс для артефактов эффект которых вызывается после подсчета кол-ва знаков определенной школы магии в заклинании
 */
public interface AfterCountSchoolMagicArtifact extends ArtifactCard {

    int afterCountSchoolMagicArtifactAction(int result, SchoolMagic schoolMagic);
}
