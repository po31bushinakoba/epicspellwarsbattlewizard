package model.cards.artifact.aftercountschoolmagic;

import controller.Game;
import model.cards.cast.SchoolMagic;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Evgeniy on 20.03.2017.
 */
public class BashmakiSkomorokha extends AbstractAfterCountSchoolMagicArtifact {

    public BashmakiSkomorokha()  {
        try {
            setIcon(ImageIO.read(new File("images/bashmakiskomorokha.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Башмаки скомороха");
        setDescription("Это сокровище считается картой со знаком кумара в каждом твоём заклинании.");
    }

    @Override
    public int afterCountSchoolMagicArtifactAction(int result, SchoolMagic schoolMagic) {
        int deltaResult = 0;
        if(schoolMagic == SchoolMagic.KUMAR){
            Game.getPlayingField().showText("Башмаки скомораха добавляют 1 знак кумара!");
            return ++deltaResult;
        }
        else return deltaResult;
    }
}
