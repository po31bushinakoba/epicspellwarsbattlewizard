package model.cards.artifact.aftercountschoolmagic;

import controller.Game;
import model.cards.artifact.ArtifactStorage;
import model.cards.cast.SchoolMagic;

/**
 * Created by Evgeniy on 25.03.2017.
 */
public class AfterCountSchoolMagicArtifactStorage extends ArtifactStorage<AfterCountSchoolMagicArtifact> {

    public int callArtifactsAction(int result, SchoolMagic schoolMagic){
        int deltaResult = 0;
        for(AfterCountSchoolMagicArtifact artifact : artifacts) {
            if(artifact.getOwner() == Game.getLastTurnedPlayer()) {
                deltaResult += artifact.afterCountSchoolMagicArtifactAction(result, schoolMagic);
            }
        }
        return deltaResult;
    }
}
