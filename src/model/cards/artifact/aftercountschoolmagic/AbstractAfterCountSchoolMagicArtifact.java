package model.cards.artifact.aftercountschoolmagic;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;

import java.util.Arrays;
import java.util.List;

/**
 * Абстрактный класс активных артефактов
 */
public abstract class AbstractAfterCountSchoolMagicArtifact extends AbstractArtifact implements AfterCountSchoolMagicArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.AFTER_COUNT_SCHOOL_MAGIC);

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }
}
