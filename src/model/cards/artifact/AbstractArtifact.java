package model.cards.artifact;

import model.cards.Card;
import model.players.Player;

/**
 * Абстрактный класс артефакта
 */
public abstract class AbstractArtifact extends Card implements ArtifactCard {

    private Player owner;

    @Override
    public Player getOwner() {
        return owner;
    }

    @Override
    public void setOwner(Player owner) {
        this.owner = owner;
    }
}
