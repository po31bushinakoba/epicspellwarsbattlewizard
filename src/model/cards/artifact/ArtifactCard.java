package model.cards.artifact;

import model.players.Player;

import java.util.List;

/**
 * Интерфес-маркер для артефактов
 */
public interface ArtifactCard {

    List<ArtifactType> getArtifactTypes();

    Player getOwner();

    void setOwner(Player owner);
}
