package model.cards.artifact.afterpowerfulldice;

import controller.Game;
import model.cards.artifact.ArtifactStorage;

/**
 * Created by Evgeniy on 25.03.2017.
 */
public class AfterPowerfullDiceArtifactStorage extends ArtifactStorage<AfterPowerfullDiceArtifact> {

    public int callArtifactsAction(int result){
        int deltaResult = 0;
        for(AfterPowerfullDiceArtifact artifact : artifacts){
            if(artifact.getOwner() == Game.getLastTurnedPlayer()) {
                deltaResult += artifact.afterPowerfullDiceArtifactAction(result);
            }
        }
        return deltaResult;
    }
}
