package model.cards.artifact.afterpowerfulldice;

import model.cards.artifact.ArtifactCard;

/**
 * Интерфейс для активных артефактов
 */
public interface AfterPowerfullDiceArtifact extends ArtifactCard{

    int afterPowerfullDiceArtifactAction(int result);
}
