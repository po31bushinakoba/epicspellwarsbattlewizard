package model.cards.artifact.afterpowerfulldice;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;

import java.util.Arrays;
import java.util.List;

/**
 * Абстрактные класс активных артефактов
 */
public abstract class AbstractAfterPowerfullDiceArtifact extends AbstractArtifact implements AfterPowerfullDiceArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.AFTER_POWERFULL_DICE);

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }
}
