package model.cards.artifact.afterpowerfulldice;

import controller.Game;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Evgeniy on 20.03.2017.
 */
public class VenecObdolbannogoKorolya extends AbstractAfterPowerfullDiceArtifact {

    public VenecObdolbannogoKorolya()  {
        try {
            setIcon(ImageIO.read(new File("images/venecobdolbannogokorolya.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Венец обдолбанного короля");
        setDescription("Добавляй 1 к результатам своих могучих бросков за каждый уникальный знак в твоём заклинании.");
    }

    @Override
    public int afterPowerfullDiceArtifactAction(int result) {
        Game.getPlayingField().showText("Венец обдолбанного короля добавляют 1 к результату могучего броска за " +
                                        "каждый уникальный знак!");
        return getOwner().getCast().getUniqueSchoolInCast();
    }
}
