package model.cards.artifact.afterpowerfulldice;

import controller.Game;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Evgeniy on 20.03.2017.
 */
public class ZolotoDurakov extends AbstractAfterPowerfullDiceArtifact {

    public ZolotoDurakov()  {
        try {
            setIcon(ImageIO.read(new File("images/zolotodurakov.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("Золото дураков");
        setDescription("Всякий раз, когда результат вражского могучего броска - 4 или меньше, накручивай себе 2 жизни.");
    }

    @Override
    public int afterPowerfullDiceArtifactAction(int result) {
        if(Game.getLastTurnedPlayer() != getOwner() && result <= 4){
            Game.getPlayingField().showText("Золото дураков даёт вам 2 жизни, т.к. результат могучего броска врага" +
                                            "меньше двух!");
            getOwner().addHitpoint(2);
        }
        return 0;
    }
}
