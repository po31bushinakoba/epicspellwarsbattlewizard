package model.cards.artifact.tryaddcardincast;

import model.cards.artifact.ArtifactStorage;
import model.cards.cast.CastCard;
import model.players.Player;

/**
 * Created by Evgeniy on 25.03.2017.
 */
public class TryAddCardInCastArtifactStorage extends ArtifactStorage<TryAddCardInCastArtifact> {

    public void callArtifactsAction(CastCard card, Player player){
        for(TryAddCardInCastArtifact artifact : artifacts){
            if(artifact.getOwner() == player) {
                artifact.tryAddCardInCastArtifactAction(card);
            }
        }
    }
}
