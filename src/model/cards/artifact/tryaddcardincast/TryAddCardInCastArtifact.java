package model.cards.artifact.tryaddcardincast;

import model.cards.artifact.ArtifactCard;
import model.cards.cast.CastCard;

/**
 * Интерфейс для активных артефактов
 */
public interface TryAddCardInCastArtifact extends ArtifactCard{

    void tryAddCardInCastArtifactAction(CastCard card);
}
