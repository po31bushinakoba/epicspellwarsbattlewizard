package model.cards.artifact.tryaddcardincast;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;

import java.util.Arrays;
import java.util.List;

/**
 * Абстрактные класс активных артефактов
 */
public abstract class AbstractTryAddCardInCastArtifact extends AbstractArtifact implements TryAddCardInCastArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.TRY_ADD_CARD_IN_CAST);

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }
}
