package model.cards.artifact;

import controller.Game;
import model.cards.artifact.aftercountcubicforpowerfulldice.BulyzhnikiOtchayaniya;
import model.cards.artifact.aftercountschoolmagic.BashmakiSkomorokha;
import model.cards.artifact.aftercountschoolmagic.Zhgufli;
import model.cards.artifact.aftercountschoolmagic.ZloveshieShlepanci;
import model.cards.artifact.afterpowerfulldice.VenecObdolbannogoKorolya;
import model.cards.artifact.afterpowerfulldice.ZolotoDurakov;
import model.cards.artifact.multiple.PetdyasytOttenkovSvinofermy;
import model.players.Player;

import java.util.*;

/**
 * Класс агрегирующий работу с артефактами. Все артефакт работают как слушатели. Каждый тип артефактов подписывается
 * на свой тип событий и слушает их. Один артефакт можно слушать несколько типов событий (агрегировать их)
 */
public class ArtifactUtil {

    private static List<AbstractArtifact> deck = new ArrayList<>();
    //все артефакты участвующие в игре
    private static List<AbstractArtifact> allArtifacts = Arrays.asList(new PetdyasytOttenkovSvinofermy(),
                                                                       new BashmakiSkomorokha(),
                                                                       new BulyzhnikiOtchayaniya(),
                                                                       new VenecObdolbannogoKorolya(),
                                                                       new Zhgufli(),
                                                                       new ZloveshieShlepanci(),
                                                                       new ZolotoDurakov());

    /**
     * Создаём колоду
     */
    public static void createDeck(){
        deck.clear();
        deck.addAll(allArtifacts);
        //удаляем те карты которые уже есть на руках у игроков
        for(Player player : Game.getPlayers()){
            for(AbstractArtifact artifact : InvolvedArtifactUtil.getArtifactsByPlayer(player)){
                deck.remove(artifact);
            }
        }
        //мешаем колоду
        Collections.shuffle(deck, Game.getRandom());
    }

    /**
     * Берем артефакт
     * @param player кто берет артефакт
     * @return получилось ли взять... по идеи всегда должно получаться (разве что их не осталось в колоде)
     */
    public static boolean takeArtifact(Player player){
        if(deck.size() == 0) createDeck();
        if(deck.size() == 0) return false;
        Game.getPlayingField().showText(player.getName() + " берет сокровище: " + deck.get(0).getName());
        InvolvedArtifactUtil.involveArtifact(deck.get(0), player);
        deck.remove(0);
        return true;
    }

    /**
     * Забрать артефакт у другого игрока
     * @param taker кто забирает
     * @param target у кого забирает
     * @param artifact что забирает
     * @return все ли прошло нормально
     */
    public static boolean takeArtifactOtherPlayer(Player taker, Player target, AbstractArtifact artifact){
        boolean success = InvolvedArtifactUtil.resetArtifact(artifact, target);
        if(success){
            Game.getPlayingField().showText(taker.getName() + " забирает у " + target.getName() + " сокровище "
                                            + artifact.getName());
            InvolvedArtifactUtil.involveArtifact(artifact, taker);
        }
        return success;
    }

    /**
     * Сбрасываем артефакт игрока
     * @param player игрок
     * @param artifact сбрасвыемы артефакт
     * @return все ли прошло нормально
     */
    public static boolean resetArtifact(Player player, AbstractArtifact artifact){
        Game.getPlayingField().showText(player.getName() + " сбрасывает сокровище: " + artifact.getName());
        return InvolvedArtifactUtil.resetArtifact(artifact, player);
    }

    public static ArtifactStorage getArtifactsByType(ArtifactType artifactType){
        return InvolvedArtifactUtil.artifactsByType.get(artifactType);
    }

    public static List<AbstractArtifact> getArtifactsByPlayer(Player player){
        return new ArrayList<>(InvolvedArtifactUtil.artifactsByPlayer.get(player));
    }

    public static Map<Player, List<AbstractArtifact>> getAllArtifactsByPlayer(){
        return InvolvedArtifactUtil.artifactsByPlayer;
    }

    /**
     * Класс, работающий с артефактыми, которые уже задействованы в игре (на руках у игроков)
     */
    private static class InvolvedArtifactUtil {

        private static Map<Player, List<AbstractArtifact>> artifactsByPlayer = new HashMap<>();
        private static Map<ArtifactType, ArtifactStorage> artifactsByType = new HashMap<>();

        static {
            for(Player player : Game.getPlayers()) artifactsByPlayer.put(player, new ArrayList<>());
            for(ArtifactType artifactType : ArtifactType.values()) artifactsByType.put(artifactType, artifactType.getArtifactStorage());
        }

        /**
         * Метод, который закрепляет артефакт за определенным игроком и вводит в игру
         * @param artifact артефакт
         * @param player игрок
         */
        public static void involveArtifact(AbstractArtifact artifact, Player player){
            artifactsByPlayer.get(player).add(artifact);
            for(ArtifactType artifactType : artifact.getArtifactTypes()) {
                artifactsByType.get(artifactType).add(artifact);
            }
            artifact.setOwner(player);
            Game.getPlayingField().showArtifacts(artifactsByPlayer);
        }

        /**
         * Метод сбрасывает определенный артефакт определенного игрока
         * @param artifact артефакт
         * @param player игрок
         */
        public static boolean resetArtifact(AbstractArtifact artifact, Player player){
            for(ArtifactType artifactType : artifact.getArtifactTypes()) {
                artifactsByType.get(artifactType).remove(artifact);
            }
            boolean result = artifactsByPlayer.get(player).remove(artifact);
            Game.getPlayingField().showArtifacts(artifactsByPlayer);
            return result;
        }

        public static List<AbstractArtifact> getArtifactsByPlayer(Player player){
            return artifactsByPlayer.get(player);
        }


    }
}
