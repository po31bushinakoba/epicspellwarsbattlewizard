package model.cards.artifact.afterpauseoff;

import model.cards.artifact.ArtifactCard;

/**
 * Интерфейс для активных артефактов
 */
public interface AfterPauseOffArtifact extends ArtifactCard{

    void afterPauseOffArtifactAction();
}
