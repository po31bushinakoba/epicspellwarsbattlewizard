package model.cards.artifact.afterpauseoff;

import controller.Game;
import model.cards.artifact.ArtifactStorage;

/**
 * Created by Evgeniy on 25.03.2017.
 */
public class AfterPauseOffArtifactStorage extends ArtifactStorage<AfterPauseOffArtifact> {

    public void callArtifactsAction(){
        for(AfterPauseOffArtifact artifact : artifacts) {
            if(artifact.getOwner() == Game.getLastTurnedPlayer()) {
                artifact.afterPauseOffArtifactAction();
            }
        }
    }
}
