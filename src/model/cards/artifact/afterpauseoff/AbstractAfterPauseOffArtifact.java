package model.cards.artifact.afterpauseoff;

import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;

import java.util.Arrays;
import java.util.List;

/**
 * Абстрактные класс активных артефактов
 */
public abstract class AbstractAfterPauseOffArtifact extends AbstractArtifact implements AfterPauseOffArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.AFTER_PAUSE_OFF);

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }
}
