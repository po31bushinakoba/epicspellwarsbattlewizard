package model.cards.artifact;

import model.cards.artifact.active.ActiveArtifactStorage;
import model.cards.artifact.aftercountcubicforpowerfulldice.AfterCountCubicForPowerfullDiceArtifactStorage;
import model.cards.artifact.aftercountschoolmagic.AfterCountSchoolMagicArtifactStorage;
import model.cards.artifact.afterpauseoff.AfterPauseOffArtifactStorage;
import model.cards.artifact.afterpowerfulldice.AfterPowerfullDiceArtifactStorage;
import model.cards.artifact.tryaddcardincast.TryAddCardInCastArtifactStorage;

/**
 * Тип артефактов
 */
public enum ArtifactType {

    ACTIVE(new ActiveArtifactStorage()), //Активный артефакт, его действие вызывается с графического интерфейса
    AFTER_COUNT_SCHOOL_MAGIC(new AfterCountSchoolMagicArtifactStorage()), //Пассивный артефакт, его действие вызывается всякий раз при подсчете знаков определенного типа
    AFTER_POWERFULL_DICE(new AfterPowerfullDiceArtifactStorage()), //пассивный, вызывается всякий раз при подсчете результата могучего броска
    AFTER_CUBIC_COUNT_FOR_POWERFULL_DICE(new AfterCountCubicForPowerfullDiceArtifactStorage()), //пассивный, вызывается при подсчете кубиков для можного броска
    TRY_ADD_CARD_IN_CAST(new TryAddCardInCastArtifactStorage()), //пассивный, вызывается при попытки (не после а можно сказать перед) добавлени карты в каст
    AFTER_PAUSE_OFF(new AfterPauseOffArtifactStorage()), //пассивный, после сняти игры с паузы
    ;


    ArtifactType(ArtifactStorage artifactStorage) {
        this.artifactStorage = artifactStorage;
    }

    private ArtifactStorage artifactStorage;

    public ArtifactStorage getArtifactStorage() {
        return artifactStorage;
    }
}
