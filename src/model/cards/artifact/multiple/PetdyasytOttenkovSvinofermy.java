package model.cards.artifact.multiple;

import controller.Game;
import model.cards.artifact.AbstractArtifact;
import model.cards.artifact.ArtifactType;
import model.cards.artifact.active.ActiveArtifact;
import model.cards.artifact.afterpauseoff.AfterPauseOffArtifact;
import model.cards.artifact.tryaddcardincast.TryAddCardInCastArtifact;
import model.cards.cast.Cast;
import model.cards.cast.CastCard;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Во время чужого хода, нажимаем на этот артефакт, он становится задействованным. После этого мы можем выбрать карту с руки,
 * которая есть у врага в заклинание и которую мы хотим сбросить, чтобы накрутить себе жизни, а врагу нанести урон.
 */
public class PetdyasytOttenkovSvinofermy extends AbstractArtifact implements ActiveArtifact, TryAddCardInCastArtifact, AfterPauseOffArtifact {

    private List<ArtifactType> artifactTypes = Arrays.asList(ArtifactType.ACTIVE, ArtifactType.TRY_ADD_CARD_IN_CAST, ArtifactType.AFTER_PAUSE_OFF);
    private boolean isActive = false;

    public PetdyasytOttenkovSvinofermy()  {
        try {
            setIcon(ImageIO.read(new File("images/petdyasyatottenkovsvinofermy.jpg")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setName("50 оттенков свинофермы");
        setDescription("Если враг играет такую же карты, как и у тебя на руке, можешь сбросить её с руки, чтобы " +
                       "нанести этому врагу 2 урона, а себе накрутить 2 жизни.");
    }

    @Override
    public List<ArtifactType> getArtifactTypes() {
        return artifactTypes;
    }

    @Override
    public void activeArtifactAction() {
        isActive = true;
    }

    @Override
    public void tryAddCardInCastArtifactAction(CastCard card) {
        if(isActive && //артефакт активирован
           Game.getLastTurnedPlayer() != null && //если равен null, то сейчас составление заклинания
           isEnemiesCastHasCardById(Game.getLastTurnedPlayer().getCast(), card) && //в заклинании врага есть такая карта
           getOwner().getCardsOnHand().remove(card)){ //у нас есть такая карта сразу удалим её
           Game.getPlayingField().showText("50 оттенков свинофермы добавили вам 2 жизни и отняли столько же врагу" +
                                            " за сброшенную карту " + card);
            getOwner().addHitpoint(2);
            Game.getLastTurnedPlayer().addHitpoint(-2);
        }
        Game.getPlayingField().showHand(Game.getPlayers(), false);
        Game.getPlayingField().showHitpoint(Game.getPlayers());
    }

    @Override
    public void afterPauseOffArtifactAction() {
        isActive = false;
    }

    /**
     * Проверяем, содержит ли каст врага определунню карту
     * @param enemiesCast каст врага
     * @param card карта
     * @return
     */
    private boolean isEnemiesCastHasCardById(Cast enemiesCast, CastCard card){
        for(CastCard enemiesCard : enemiesCast.getCast()){
            if(enemiesCard.getId() == card.getId()) return true;
        }
        return false;
    }
}
