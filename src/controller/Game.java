package controller;

import model.cards.artifact.ArtifactUtil;
import model.cards.artifact.aftercountcubicforpowerfulldice.AfterCountCubicForPowerfullDiceArtifactStorage;
import model.cards.artifact.afterpauseoff.AfterPauseOffArtifactStorage;
import model.cards.artifact.afterpowerfulldice.AfterPowerfullDiceArtifactStorage;
import model.cards.cast.*;
import model.players.*;
import view.PlayingField;
import view.PlayingFieldWithHuman2Player;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

import static model.cards.artifact.ArtifactType.AFTER_CUBIC_COUNT_FOR_POWERFULL_DICE;
import static model.cards.artifact.ArtifactType.AFTER_PAUSE_OFF;
import static model.cards.artifact.ArtifactType.AFTER_POWERFULL_DICE;

/**
 * Главный класс игры
 */
public class Game {

    private static List<Player> players = Arrays.asList(new Human(), new BrainBot());
    private static PlayingField playingField = new PlayingFieldWithHuman2Player();
    private static Random random = new Random();
    private static Player lastTurnedPlayer;
    private static volatile CountDownLatch lock;

    //TODO отображать на игровом поле действие которое делает карта
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        playingField.showHitpoint(players);

        Player winner;
        while((winner = winner(players)) == null){
            lastTurnedPlayer = null;

            CardCastUtil.dealCards();

            playingField.showHand(players, true);

            List<FutureTask<Cast>> tasks = new ArrayList<>();
            //игроки ходят
            for(Player player : players){
                FutureTask<Cast> task = new FutureTask<>(player::turn);
                tasks.add(task);
                new Thread(task).start();
            }

            //список заклинаний которые будут сыграны в этом раунде отсортированные количеству карт и инициативе
            List<Cast> casts = new ArrayList<>();
            for(FutureTask<Cast> task : tasks){
                casts.add(task.get());
            }

            //в первую очередю сортируем по количеству карт в касте и инициативе
            casts.sort((cast1, cast2) -> {
                if(cast1.getCast().size() != cast2.getCast().size()) return cast1.getCast().size() - cast2.getCast().size();
                return cast2.getInitiative() - cast1.getInitiative();
            });
            //но нужно пробежаться еще раз и до сортировать касты с одинаковыми параметрами путем бросания кубика
            for(int i = 0; i < casts.size() - 1; i++){
                if(casts.get(i).getCast().size() == casts.get(i + 1).getCast().size() &&
                   casts.get(i).getInitiative() == casts.get(i + 1).getInitiative()){
                    int cubicResult1, cubicResult2;
                    do{
                        cubicResult1 = rollDice(casts.get(i).getPlayer());
                        cubicResult2 = rollDice(casts.get(i + 1).getPlayer());
                    } while (cubicResult1 == cubicResult2);
                    if(cubicResult2 > cubicResult1){
                        Collections.swap(casts, i, i + 1);
                    }
                }
            }

            for(Cast cast : casts) cast.getPlayer().getCardsOnHand().removeAll(cast.getCast());
            playingField.showHand(players, false);

            for(Cast cast : casts){
                if(cast.getPlayer().getHitpoint() <= 0) continue;
                lastTurnedPlayer = cast.getPlayer();
                cast.play(players);
                playingField.showChanges(players);
            }
        }

        if(winner instanceof Trainer){
            ((Trainer) winner).saveInFile();
        }
        for(Player player : players){
            if(player instanceof Trainer){
                ((Trainer) player).clearLessons();
            }
        }
        System.out.println(winner);

        playingField.showWinner(winner);
    }

    /**
     * Определяет победителя
     * @param players список игроков
     * @return победитель, если есть или null
     */
    private static Player winner(List<Player> players){
        int playersCount = players.size();
        for(Player player : players){
            if(player.getHitpoint() <= 0) playersCount--;
        }
        if(playersCount <= 1) return lastTurnedPlayer;
        else return null;
    }

    /**
     * Игрок кидает кость
     * @param player
     */
    public static int rollDice(Player player){
        int result = 1 + random.nextInt(6);
        playingField.showRollDices(Arrays.asList(result), player);
        pauseOn();
        return result;
    }

    /**
     * Мощный бросок
     * @param cast по какому заклинанию бросаем
     * @param schoolMagic по какому знаку бросок
     * @return сколько выпало
     */
    public static int powerfullRollDices(Cast cast, SchoolMagic schoolMagic){
        int countCubic = cast.getCountSchoolMagicByTypeInCast(schoolMagic);
        countCubic += ((AfterCountCubicForPowerfullDiceArtifactStorage) ArtifactUtil
                .getArtifactsByType(AFTER_CUBIC_COUNT_FOR_POWERFULL_DICE)).callArtifactsAction(countCubic);
        int sumResult = 0;
        List<Integer> results = new ArrayList<>();
        for(int i = 0; i < countCubic; i++){
            int cubicResult = 1 + random.nextInt(6);
            sumResult += cubicResult;
            results.add(cubicResult);
        }
        playingField.showRollDices(results, cast.getPlayer());
        sumResult += ((AfterPowerfullDiceArtifactStorage) ArtifactUtil
                .getArtifactsByType(AFTER_POWERFULL_DICE)).callArtifactsAction(sumResult);
        pauseOn();
        return sumResult;
    }

    /**
     * Ищем самых живучих врагов игрока
     * @param me
     * @return
     */
    public static List<Player> mostAliveEnemies(Player me){
        int maxHitpoint = 0;
        List<Player> playersWithMaxHitpoint = new ArrayList<>();
        for(Player player : players){
            if(!player.equals(me)){
                if(player.getHitpoint() == maxHitpoint) playersWithMaxHitpoint.add(player);
                else if(player.getHitpoint() > maxHitpoint){
                    playersWithMaxHitpoint.clear();
                    playersWithMaxHitpoint.add(player);
                    maxHitpoint = player.getHitpoint();
                }
            }
        }
        return playersWithMaxHitpoint;
    }

    /**
     * Ищем игроков живучей me
     * @param me игрок
     * @return
     */
    public static List<Player> alivestMeEnemies(Player me){
        return players.stream().filter(player -> player.getHitpoint() > me.getHitpoint()).collect(Collectors.toList());
    }

    public static PlayingField getPlayingField() {
        return playingField;
    }

    public static Random getRandom() {
        return random;
    }

    /**
     * Ждем пока кто-то извне не прервет ожидание
     */
    public static void pauseOn(){
        lock = new CountDownLatch(1);
        playingField.pause();
        try {
            lock.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Прерываем ожидание
     */
    public static void pauseOff(){
        ((AfterPauseOffArtifactStorage) ArtifactUtil.getArtifactsByType(AFTER_PAUSE_OFF)).callArtifactsAction();
        lock.countDown();
    }

    public static List<Player> getPlayers() {
        return players;
    }

    public static Player getLastTurnedPlayer() {
        return lastTurnedPlayer;
    }

    public static Human getHumanPlayer() {
        for (Player player : players) {
            if (player instanceof Human) return (Human) player;
        }
        return null;
    }
}
