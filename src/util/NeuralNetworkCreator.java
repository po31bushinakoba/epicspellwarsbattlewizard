package util;

import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Класс создания нейронной сети на основе уроков
 */
public class NeuralNetworkCreator {

    //Random number generator seed, for reproducability
    public static final int seed = 0;
    //Number of iterations per minibatch
    public static final int iterations = 1;
    //Number of epochs (full passes of the data)
    public static final int nEpochs = 2000;
    //Batch size: i.e., each epoch has nSamples/batchSize parameter updates
    public static final int batchSize = 100; //в 10 раз меньше samples;
    //Network learning rate
    public static final double learningRate = 0.01;

    public static final Random rng = new Random(seed);

    public static void main(String[] args) {
        //Generate the training data
        DataSetIterator iterator = getTrainingData(batchSize,rng);

        //Create the network
        int numInputs = 2;
        int numOutputs = 16;
        int numHiddenNodes = 20;
        MultiLayerNetwork net = new MultiLayerNetwork(new NeuralNetConfiguration.Builder()
                                                              .seed(seed)
                                                              .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT) // use stochastic gradient descent as an optimization algorithm
                                                              .iterations(iterations)
                                                              .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                                                              .learningRate(learningRate)
                                                              .weightInit(WeightInit.XAVIER)
                                                              .updater(Updater.NESTEROVS).momentum(0.9)
                                                              .list()
                                                              .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                                                                                                .activation(Activation.TANH).build())
                                                              .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
                                                                                                .activation(Activation.TANH).build())
                                                              .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                                                                      .activation(Activation.IDENTITY)
                                                                      .nIn(numHiddenNodes).nOut(numOutputs).build())
                                                              .pretrain(false).backprop(true).build());
        net.init();
        net.setListeners(new ScoreIterationListener(1));


        //Train the network on the full data set, and evaluate in periodically
        for( int i=0; i<nEpochs; i++ ){
            iterator.reset();
            net.fit(iterator);
        }

        try {
            ModelSerializer.writeModel(net, "nn/net.zip", true);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Test
        for(int i = 1; i <= 20; i++) {
            for(int j = 1; j <= 20; j++) {
                INDArray input = Nd4j.create(new double[]{i, j}, new int[]{1, 2});
                INDArray out = net.output(input, false);
                System.out.println(out);
            }
        }

    }

    private static DataSetIterator getTrainingData(int batchSize, Random rand){

        List<Float> input1 = new ArrayList();
        List<Float> input2 = new ArrayList();
        List<Float> output1 = new ArrayList();
        List<Float> output2 = new ArrayList();
        List<Float> output3 = new ArrayList();
        List<Float> output4 = new ArrayList();
        List<Float> output5 = new ArrayList();
        List<Float> output6 = new ArrayList();
        List<Float> output7 = new ArrayList();
        List<Float> output8 = new ArrayList();
        List<Float> output9 = new ArrayList();
        List<Float> output10 = new ArrayList();
        List<Float> output11 = new ArrayList();
        List<Float> output12 = new ArrayList();
        List<Float> output13 = new ArrayList();
        List<Float> output14 = new ArrayList();
        List<Float> output15 = new ArrayList();
        List<Float> output16 = new ArrayList();

        try(BufferedReader br = new BufferedReader(new FileReader(new File("nn/lessons")))){
            String line;
            boolean hit = true;
            while((line = br.readLine()) != null){
                String[] splitLine = line.split(" ");
                if(hit){
                    input1.add(Float.valueOf(splitLine[0]));
                    input2.add(Float.valueOf(splitLine[1]));
                } else {
                    output1.add(Float.valueOf(splitLine[0]));
                    output2.add(Float.valueOf(splitLine[1]));
                    output3.add(Float.valueOf(splitLine[2]));
                    output4.add(Float.valueOf(splitLine[3]));
                    output5.add(Float.valueOf(splitLine[4]));
                    output6.add(Float.valueOf(splitLine[5]));
                    output7.add(Float.valueOf(splitLine[6]));
                    output8.add(Float.valueOf(splitLine[7]));
                    output9.add(Float.valueOf(splitLine[8]));
                    output10.add(Float.valueOf(splitLine[9]));
                    output11.add(Float.valueOf(splitLine[10]));
                    output12.add(Float.valueOf(splitLine[11]));
                    output13.add(Float.valueOf(splitLine[12]));
                    output14.add(Float.valueOf(splitLine[13]));
                    output15.add(Float.valueOf(splitLine[14]));
                    output16.add(Float.valueOf(splitLine[15]));
                }
                hit = !hit;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        INDArray inputNDArray = Nd4j.hstack(Nd4j.create(input1.stream().mapToDouble(Float::floatValue).toArray(), new int[]{input1.size(), 1}),
                                            Nd4j.create(input2.stream().mapToDouble(Float::floatValue).toArray(), new int[]{input2.size(), 1}));
        INDArray outputNDArray = Nd4j.hstack(Nd4j.create(output1.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output1.size(), 1}),
                                             Nd4j.create(output2.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output2.size(), 1}),
                                             Nd4j.create(output3.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output3.size(), 1}),
                                             Nd4j.create(output4.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output4.size(), 1}),
                                             Nd4j.create(output5.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output5.size(), 1}),
                                             Nd4j.create(output6.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output6.size(), 1}),
                                             Nd4j.create(output7.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output7.size(), 1}),
                                             Nd4j.create(output8.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output8.size(), 1}),
                                             Nd4j.create(output9.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output9.size(), 1}),
                                             Nd4j.create(output10.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output10.size(), 1}),
                                             Nd4j.create(output11.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output11.size(), 1}),
                                             Nd4j.create(output12.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output12.size(), 1}),
                                             Nd4j.create(output13.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output13.size(), 1}),
                                             Nd4j.create(output14.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output14.size(), 1}),
                                             Nd4j.create(output15.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output15.size(), 1}),
                                             Nd4j.create(output16.stream().mapToDouble(Float::floatValue).toArray(), new int[]{output16.size(), 1}));
        DataSet dataSet = new DataSet(inputNDArray, outputNDArray);
        List<DataSet> listDs = dataSet.asList();
        Collections.shuffle(listDs, rng);
        return new ListDataSetIterator(listDs, batchSize);

    }
}
